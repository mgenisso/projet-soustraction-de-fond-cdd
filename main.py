import os
import soustraction
import argparse as ap
import tkinter as tk
import gui

parser = ap.ArgumentParser(description="Soustraction de fond")
parser.add_argument('--camera_number', metavar='camera', type=str, help='choisi la camera a traiter')
parser.add_argument('--color', metavar='color', type=str, help='grey ou rbg pour la soustraction')
parser.add_argument('--epsilon', metavar='epsilon', type=int, help='parametre pour la soustraction')
parser.add_argument('--precalc_background', metavar='precalc', type=bool, help='true pour calculer le background moyen de la scene')
parser.add_argument('--graphic_mode', metavar='show', type=bool, help='lance la GUI')
args = parser.parse_args()


def main():
    if args.graphic_mode is True:
        gui.display()
    else:
        if args.precalc_background is True:
            print('calcul du background moyen pour chaque scene')
            precalculate_background()
        if args.epsilon is None or args.epsilon is not int :
            print('il faut preciser une valeur entiere epsilon pour la soustraction')
        if args.color is None:
            print('il faut preciser une couleur pour le rendu, grey ou rgb')
        if args.color != 'grey' or args.color != 'rgb':
            args.color = 'grey'
        if args.camera_number is None :
            print('il faut preciser un numero de camera entre 0 et 99 pour lancer le batch')
        if args.epsilon is not None and args.color is not None and args.camera_number is not None :
            print('lancement du batch, camera ' + args.camera_number + ', color ' + args.color + ', epsilon = ' + str(args.epsilon) )
            soustraction.batch_mode(args.camera_number, args.color, args.epsilon)
            print('batch fini pour la camera')



if __name__== "__main__":
    main()
