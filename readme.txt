librairie nécessaire pour éxécution:
opencv-python
tkinter
pillow
argsparse
numpy

le fichier main ainsi que les autres fichier du programme doivent être placé
dans un dossier qui contient également les données fournies pour l'exercice
(c'est la limitation principale du programme faute de temps)

Dans la session graphique, il y a une case vide qui correspond à une valeur d'epsilon
pour lancer un batch, par défaut epsilon=20
Si une image ne s'affiche pas en cliquant sur les boutons de la session graphique,
c'est que le batch correspondant n'a pas été calculé.

Pour lancer la session graphique :

python3 main.py --graphic_mode True

Pour lancer en ligne de commande un batch sur une caméra:

python3  main.py --camera_number <int_cam> --color <color> --epsilon <epsilon>

<int_cam> un entier à deux chiffres qui doit être présent dans les caméras du fichier
<color> pour la couleur de rendu, doit être grey ou rgb
<epsilon> un entier pour définir la sélectivité de l'algorithme de Soustraction

Pour précalculer les fonds moyen avant de faire un batch:
python3  main.py --precalc_background True

si le précalcul n'est pas fait avant, il sera fait automatiquement avant le batch
