import os
import cv2 as cv
import numpy as np

def batch_mode(camera, color, epsilon):
    print('starting batch for cam-0' + camera)
    background = 'fond_precalc/' + 'cam-0' + camera + '/precaculated.png'
    photo_dir =  'ami-own-jog-cam-0x7/cam-0' + camera
    if cv.imread(background) is None :
        precalculate_background()
    for id in os.scandir(photo_dir):
        if id.is_file():
            photo_id = str(id)[11:25]
            photo_id = photo_dir + '/' + photo_id
            soustract(background, photo_id, color, epsilon)


def soustract(background, image, color, epsilon):
    fond = cv.imread(background)
    img = cv.imread(image)
    grey_fond = cv.cvtColor(fond, cv.COLOR_BGR2GRAY)
    grey_img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    if(color=='grey'):
        result = np.zeros((2048,2048), np.uint8)
        for i in range(2048):
            for j in range(2048):
                if grey_fond[i][j] + epsilon < grey_img[i][j]:
                    result[i][j] = 255
                elif grey_fond[i][j] - epsilon > grey_img[i][j]:
                    result[i][j] = 255
                else :
                    result[i][j] = 0
        save_image(image, result, 'grey')
    if(color=='rgb'):
        result = np.zeros((2048,2048,3), np.uint8)
        for i in range(2048):
            for j in range(2048):
                if grey_fond[i][j] + epsilon < grey_img[i][j]:
                    result[i][j] = img[i][j]
                elif grey_fond[i][j] - epsilon > grey_img[i][j]:
                    result[i][j] = img[i][j]
                else :
                    result[i][j] = [0,0,0]
        save_image(image, result,'rgb')


def precalculate_background():
    directory = 'bg3-cam-0x7'
    if not os.path.exists('fond_precalc'):
        os.makedirs('fond_precalc')
    for dir in os.scandir(directory):
        strdir = str(dir)[11:18]
        if not os.path.exists('fond_precalc/' + strdir):
            os.makedirs('fond_precalc/' + strdir)
        result = np.zeros((2048,2048,3))
        count = 0
        for filename in os.scandir(dir):
            if filename.is_file():
                fond = cv.imread(os.path.join(filename))
                result = result + fond
                count += 1
        for i in range(2048):
            for j in range(2048):
                result[i][j][0] = int(result[i][j][0] / count)
                result[i][j][1] = int(result[i][j][1] / count)
                result[i][j][2] = int(result[i][j][2] / count)
        cv.imwrite('fond_precalc/' + strdir + '/' + 'precaculated.png', result)


def save_image(path, image, color):
    saving_path = 'wt_background' + color
    if not os.path.exists(saving_path):
        os.makedirs(saving_path)
    saving_path = saving_path + path[19:27]
    if not os.path.exists(saving_path):
        os.makedirs(saving_path)
    saving_path = saving_path + path[27:]
    print(saving_path)
    cv.imwrite(saving_path, image)
