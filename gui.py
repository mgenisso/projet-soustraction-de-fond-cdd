import os
import cv2 as cv
import tkinter as tk
from PIL import Image,ImageTk
import soustraction


fond_path = 'fond_precalc/'
rgb_path = 'wt_backgroundrgb/'
grey_path = 'wt_backgroundgrey/'
nofilter_path = 'ami-own-jog-cam-0x7/'
mode = ['with_background','wt_background_rgb','wt_background_grey']
frame = [filename for filename in os.listdir('ami-own-jog-cam-0x7/cam-007')]
frame.sort()
cam = [camname for camname in os.listdir('ami-own-jog-cam-0x7')]
cam.remove('cam07.mp4')
cam.sort()

#définition de la classe qui sert à l'affichage graphique
class App(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title('Tkinter PhotoImage Demo')
        self.geometry('1200x1000')
        self.color = 'grey'
        self.epsilon = ''
        self.epsilon = 20
        self.trame_iter = 0
        self.colorlist = tk.StringVar(value=['rgb','grey'])
        self.camvar = tk.StringVar(value=cam)
        self.modelist=tk.StringVar(value=mode)
        self.mode = 'with_background'
        self.current_mode = nofilter_path
        self.current_cam = cam[0]
        self.current_img = frame[self.trame_iter]
        self.img = Image.open(self.current_mode + self.current_cam + '/' + self.current_img)
        self.resizedimg = self.img.resize((1000,1000))
        self.image = ImageTk.PhotoImage(self.resizedimg)
        self.label = tk.Label(self, image=self.image)
        self.label.pack(side = 'left')

        #listbox for view_mode
        self.listbox_mode = tk.Listbox(self,listvariable = self.modelist ,selectmode='browse')
        self.listbox_mode.bind('<<ListboxSelect>>', self.switch_mode)
        self.listbox_mode.pack()

        #listbox for cameras
        self.listbox_cam = tk.Listbox(self,listvariable = self.camvar ,selectmode='browse')
        self.listbox_cam.bind('<<ListboxSelect>>', self.switch_cam)
        self.listbox_cam.pack()


       # frame_forward_switch_button
        self.button_switch_frame_fw = tk.Button(self, text='Trame suivante')
        self.button_switch_frame_fw['command'] = self.change_trame_forward
        self.button_switch_frame_fw.pack()

       # frame_backward_switch_button
        self.button_switch_frame_bw = tk.Button(self, text='Trame precedente')
        self.button_switch_frame_bw['command'] = self.change_trame_backward
        self.button_switch_frame_bw.pack()


       #epsilon entry
        self.entry = tk.Entry(self, textvariable=self.epsilon)
        self.entry.pack()

        #listbox for cameras
        self.listbox_color = tk.Listbox(self,listvariable = self.colorlist ,selectmode='browse')
        self.listbox_color.bind('<<ListboxSelect>>', self.switch_color)
        self.listbox_color.pack()

       #launch new batch with new epsilon
        self.button_switch_color = tk.Button(self, text='Nouveau Batch')
        self.button_switch_color['command'] = self.new_batch
        self.button_switch_color.pack()


    def change_trame_backward(self):
        self.trame_iter -=1
        if self.trame_iter <= -len(frame):
            self.trame_iter = len(frame)-1
        self.current_img = frame[self.trame_iter]
        self.update_image()

    def change_trame_forward(self):
        self.trame_iter +=1
        if self.trame_iter >= len(frame):
            self.trame_iter = -len(frame)
        self.current_img = frame[self.trame_iter]
        self.update_image()

    def switch_mode(self, event):
        self.indicemode = self.listbox_mode.curselection()
        self.mode = self.listbox_mode.get(self.indicemode)
        if self.mode == 'with_background':
            self.current_mode = 'ami-own-jog-cam-0x7/'
        elif self.mode == 'wt_background_grey':
            self.current_mode = 'wt_backgroundgrey/'
        elif self.mode == 'wt_background_rgb':
            self.current_mode = 'wt_backgroundrgb/'
        self.update_image()

    def switch_cam(self, event):
        self.indice = self.listbox_cam.curselection()
        self.current_cam = self.listbox_cam.get(self.indice)
        self.update_image()

    def switch_color(self, event):
        self.indicecolor = self.listbox_color.curselection()
        self.color = self.listbox_color.get(self.indicecolor)

    def new_batch(self):
        soustraction.batch_mode(self.current_cam[4:], self.color, self.epsilon)


    def update_image(self):
        self.img = Image.open(self.current_mode + self.current_cam + '/' + self.current_img)
        self.resizedimg = self.img.resize((1000,1000))
        self.image = ImageTk.PhotoImage(self.resizedimg)
        self.label.configure(image=self.image)



def display():
    gui = App()
    gui.mainloop()
